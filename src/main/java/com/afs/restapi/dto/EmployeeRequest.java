package com.afs.restapi.dto;

public class EmployeeRequest {
    private Integer salary;

    private Integer age;

    public EmployeeRequest() {

    }


    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
