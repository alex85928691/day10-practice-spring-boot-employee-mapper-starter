package com.afs.restapi.controller;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    private final EmployeeMapper employeeMapper;
    public EmployeeController(EmployeeService employeeService, EmployeeMapper employeeMapper) {
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployees() {
        return employeeMapper.toListOfResponse(employeeService.findAll());
    }

    //TODO
    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {
        Employee employee =  employeeService.findById(id);
        return employeeMapper.toResponse(employee);
    }

    //TODO
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateEmployee(@PathVariable Long id, @RequestBody EmployeeRequest employeeRequest) {
        Employee employee = employeeMapper.toEntity(employeeRequest);
        employeeService.update(id, employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @GetMapping(params = "gender")
    public List<EmployeeResponse> getEmployeesByGender(@RequestParam String gender) {
        List<Employee> employeeList = employeeService.findAllByGender(gender);
        return employeeMapper.toListOfResponse(employeeList);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeService.create(employee);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<EmployeeResponse> findEmployeesByPage(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        List<Employee> employeeList = employeeService.findByPage(pageNumber, pageSize);
        return employeeMapper.toListOfResponse(employeeList);
    }

}
