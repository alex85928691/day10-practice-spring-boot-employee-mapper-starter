package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyMapper {

    @Autowired
    private EmployeeMapper employeeMapper;

    private CompanyService companyService;

    public CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();

        companyResponse.setId(company.getId());
        companyResponse.setName(company.getName());
        List<EmployeeResponse> employeeList = company.getEmployees().stream()
                .map(employeeMapper::toResponse)
                .collect(Collectors.toList());
        companyResponse.setEmployees(employeeList);

        return companyResponse;
    }

    public Company toEntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest,company);
        return company;
    }

    public List<CompanyResponse> toResponseList(List<Company> companyList) {
        return companyList.stream().map(this::toResponse).collect(Collectors.toList());
    }


    public List<EmployeeResponse> toResponseEmployees(List<Employee> employeeList) {
        return employeeList.stream().map(employee -> employeeMapper.toResponse(employee)).collect(Collectors.toList());
    }
}
